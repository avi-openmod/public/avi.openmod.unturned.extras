using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.Extensions.DependencyInjection;

using OpenMod.API.Ioc;
using OpenMod.API.Permissions;
using OpenMod.Core.Permissions;
using OpenMod.Unturned.Users;

namespace Avi.OpenMod.Unturned.Extras.Services.RoleDataHelper; 

[ServiceImplementation(Lifetime = ServiceLifetime.Scoped)]
// ReSharper disable once UnusedType.Global
internal class RoleDataHelper : IRoleDataHelper {

    private readonly IUnturnedUserDirectory _userDirectory;
    private readonly IPermissionRoleStore _permissionRoleStore;
    private readonly IPermissionRolesDataStore _permissionRolesDataStore;

    public RoleDataHelper(
        IUnturnedUserDirectory userDirectory,
        IPermissionRoleStore permissionRoleStore,
        IPermissionRolesDataStore permissionRolesDataStore
    ) {
        _userDirectory = userDirectory;
        _permissionRoleStore = permissionRoleStore;
        _permissionRolesDataStore = permissionRolesDataStore;
    }

    public async Task<T?> GetRoleDataAsync<T>(IPermissionActor actor, string key) {

        IReadOnlyCollection<IPermissionRole> userRoles = await _permissionRoleStore.GetRolesAsync(actor);
        foreach (IPermissionRole role in userRoles.OrderByDescending(x => x.Priority)) {
            (bool yes, T? tResult) = await GetDataFromPermissionRole<T>(role, key);
            if (yes) {
                return tResult;
            }
        }

        return default;
    }

    public async Task<Dictionary<UnturnedUser, T>> GetRoleDataAsync<T>(string key) {

        var result = new Dictionary<UnturnedUser, T>();

        IReadOnlyCollection<IPermissionRole> roles = await _permissionRoleStore.GetRolesAsync();
        if (!roles.Any()) {
            return result;
        }

        var slotsByRole = new Dictionary<string, T>();
        foreach (IPermissionRole role in roles) {
            (bool yes, T? tResult) = await GetDataFromPermissionRole<T>(role, key);
            if (yes) {
                slotsByRole[role.Id] = tResult!;
            }
        }

        foreach (UnturnedUser user in _userDirectory.GetOnlineUsers()) {
            IReadOnlyCollection<IPermissionRole> userRoles = await _permissionRoleStore.GetRolesAsync(user);
            T? userResult = userRoles
                .OrderByDescending(x => x.Priority)
                .Select(role => slotsByRole.TryGetValue(role.Id, out T t) ? t : default)
                .FirstOrDefault(ConversionExtensions.IsNotDefault);
            if (userResult.IsNotDefault()) {
                result[user] = userResult!;
            }
        }

        return result;
    }

    private async Task<(bool, T?)> GetDataFromPermissionRole<T>(IPermissionRole role, string key) {

        string[] split = key.Split(':');
        if (split.Length == 1) {
            var data = await _permissionRolesDataStore.GetRoleDataAsync<T>(role.Id, key);
            if (data.IsNotDefault()) {
                return (true, data);
            }
        } else {
            var data = await _permissionRolesDataStore.GetRoleDataAsync<IDictionary<object, object>>(role.Id, split[0]);
            if (data.IsNotDefault()) {
                var rd = new RoleData(data!);
                var result = rd.GetSubData<T>(string.Join("", split.Skip(1)));
                if (result != null) {
                    return (true, result);
                }
            }
        }

        return (false, default);
    }

}

internal static class ConversionExtensions {

    public static bool IsNotDefault<T>(this T? value)
        => !EqualityComparer<T?>.Default.Equals(default, value);

}