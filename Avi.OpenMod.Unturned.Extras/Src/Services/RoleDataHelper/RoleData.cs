using System.Collections.Generic;
using System.Linq;

using OpenMod.Core.Persistence;

using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace Avi.OpenMod.Unturned.Extras.Services.RoleDataHelper; 

internal class RoleData {

    private readonly IDictionary<object, object> _data;

    public RoleData(IDictionary<object, object> data)
        => _data = data;

    public T? GetSubData<T>(string key) {

        string[] split = key.Split(':');
        string firstKeyPart = split[0];
        if (split.Length == 1) {
            return _data.GetValue<T>(firstKeyPart);
        }

        IDictionary<object, object>? subDic = _data.GetSubDictionary(firstKeyPart);
        if (subDic != null) {
            return new RoleData(subDic).GetSubData<T>(string.Join("", split.Skip(1)));
        }

        return default;
    }

}

internal static class DictionaryExtensions {

    private static ISerializer? _serializer;

    private static IDeserializer? _deserializer;

    private static ISerializer Serializer => _serializer ??= new SerializerBuilder()
        .WithNamingConvention(CamelCaseNamingConvention.Instance)
        .WithTypeConverter(new YamlNullableEnumTypeConverter())
        .DisableAliases()
        .Build();
    private static IDeserializer Deserializer => _deserializer ??= new DeserializerBuilder()
        .WithNamingConvention(CamelCaseNamingConvention.Instance)
        .IgnoreUnmatchedProperties()
        .WithTypeConverter(new YamlNullableEnumTypeConverter())
        .Build();

    public static IDictionary<object, object>? GetSubDictionary(this IDictionary<object, object> data, string key)
        => data.TryGetValue(key, out object value) && value is IDictionary<object, object> result ? result : default;

    public static T? GetValue<T>(this IDictionary<object, object> data, string key) {

        if (!data.TryGetValue(key, out object value)) {
            return default;
        }

        switch (value) {

            case T obj: {
                return obj;
            }

            case null: {
                return default;
            }

            default: {
                string serialized = Serializer.Serialize(value);
                return Deserializer.Deserialize<T>(serialized);
            }

        }

    }

}