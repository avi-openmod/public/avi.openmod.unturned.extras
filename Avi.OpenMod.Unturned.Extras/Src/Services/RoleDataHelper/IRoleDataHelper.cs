using System.Collections.Generic;
using System.Threading.Tasks;

using OpenMod.API.Ioc;
using OpenMod.API.Permissions;
using OpenMod.Unturned.Users;

// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedMember.Global
// ReSharper disable UnusedType.Global

namespace Avi.OpenMod.Unturned.Extras.Services.RoleDataHelper; 

/// <summary>
/// Service that allows finer controls over role data
/// </summary>
[Service]
public interface IRoleDataHelper {

    /// <param name="actor">Actor to get role data for</param>
    /// <param name="key">Role data key. This can include ':' to get sub data</param>
    /// <typeparam name="T">Return type</typeparam>
    /// <returns>Role data for specified key, or default value if no data available for the key</returns>
    Task<T?> GetRoleDataAsync<T>(IPermissionActor actor, string key);

    /// <param name="key">Role data key. This can include ':' to get sub data</param>
    /// <typeparam name="T">Return type</typeparam>
    /// <returns>Dictionary of available role data for all online users at specified key.
    /// Result only includes an entry for a user if role data exists for that user</returns>
    Task<Dictionary<UnturnedUser, T>> GetRoleDataAsync<T>(string key);

}