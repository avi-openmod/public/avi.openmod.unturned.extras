using System;

using Autofac;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

using OpenMod.API;
using OpenMod.API.Ioc;

using SDG.Unturned;

namespace Avi.OpenMod.Unturned.Extras.Services.WorkshopChecker; 

[ServiceImplementation(Lifetime = ServiceLifetime.Scoped)]
// ReSharper disable once UnusedType.Global
internal class WorkshopEffectChecker : IWorkshopEffectChecker {

    #region Services and ctor

    private readonly ILogger<WorkshopEffectChecker> _logger;
    private readonly Lazy<IOpenModComponent> _lazyComponent;

    private IOpenModComponent Component => _lazyComponent.Value;

    public WorkshopEffectChecker(
        ILogger<WorkshopEffectChecker> logger,
        Lazy<IOpenModComponent> lazyComponent
    ) {
        _logger = logger;
        _lazyComponent = lazyComponent;
    }

    #endregion

    #region Implementations

    public bool IsEffectLoaded(ushort id)
        => Assets.find(EAssetType.EFFECT, id) is EffectAsset;

    public bool IsEffectLoaded(string key) {

        var configuration = Component.LifetimeScope.ResolveOptional<IConfiguration>();
        if (configuration == null) {
            _logger.LogError("Component '{ComponentId}' doesn't have a config to resolve effect ID from", Component.OpenModComponentId);
            return false;
        }

        ushort? effectId = configuration.GetValue<ushort?>($"workshop:effect_ids:{key}");
        if (effectId == null) {
            _logger.LogError("Effect ID for '{EffectName}' is not defined in config for component '{ComponentId}'",
                key, Component.OpenModComponentId);
            return false;
        }

        return IsEffectLoaded(effectId.Value);
    }

    #endregion

}