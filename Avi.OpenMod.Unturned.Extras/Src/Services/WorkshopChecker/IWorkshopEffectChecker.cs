using OpenMod.API.Ioc;

// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedMember.Global
// ReSharper disable UnusedType.Global
// ReSharper disable UnusedMemberInSuper.Global

namespace Avi.OpenMod.Unturned.Extras.Services.WorkshopChecker; 

/// <summary>
/// Service that allows checking whether workshop effects are loaded
/// </summary>
[Service]
public interface IWorkshopEffectChecker {

    /// <returns>true if effect asset is loaded for specified id</returns>
    bool IsEffectLoaded(ushort id);

    /// <returns>true if effect asset is loaded for specified key in config at "workshop:effect_ids:key".
    /// This overload only works for components that have configuration available</returns>
    bool IsEffectLoaded(string key);

}