// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedType.Global
// ReSharper disable UnusedMember.Global
// ReSharper disable StringLiteralTypo

namespace Avi.OpenMod.Unturned.Extras.Extensions; 

// Original: https://gitlab.com/-/snippets/2031682
/// 
public static class UnityRichTextRemoverExtensions {

    private static readonly string[] RichText = {
        "b",
        "i",
        // TMP
        "u",
        "s",
        "sup",
        "sub",
        "allcaps",
        "smallcaps",
        "uppercase",
    };
    private static readonly string[] RichTextDynamic = {
        "color",
        // TMP
        "align",
        "size",
        "cspace",
        "font",
        "indent",
        "line-height",
        "line-indent",
        "link",
        "margin",
        "margin-left",
        "margin-right",
        "mark",
        "mspace",
        "noparse",
        "nobr",
        "page",
        "pos",
        "space",
        "sprite index",
        "sprite name",
        "sprite",
        "style",
        "voffset",
        "width",
    };

    /// <returns>Text with stripped Unity/TextMeshPro rich text tags</returns>
    public static string RemoveRichText(this string input) {

        foreach (string tag in RichTextDynamic) {
            input = RemoveRichTextDynamicTag(input, tag);
        }

        foreach (string tag in RichText) {
            input = RemoveRichTextTag(input, tag);
        }

        return input;
    }

    private static string RemoveRichTextDynamicTag(string input, string tag) {
        // ReSharper disable once TooWideLocalVariableScope
        int index;
        while (true) {
            // ReSharper disable once StringIndexOfIsCultureSpecific.1
            index = input.IndexOf($"<{tag}=");
            if (index != -1) {
                int endIndex = input.Substring(index, input.Length - index).IndexOf('>');
                if (endIndex > 0) {
                    input = input.Remove(index, endIndex + 1);
                }

                continue;
            }

            input = RemoveRichTextTag(input, tag, false);
            return input;
        }
    }

    private static string RemoveRichTextTag(string input, string tag, bool isStart = true) {
        while (true) {
            // ReSharper disable once StringIndexOfIsCultureSpecific.1
            int index = input.IndexOf(isStart ? $"<{tag}>" : $"</{tag}>");
            if (index != -1) {
                input = input.Remove(index, 2 + tag.Length + (!isStart).GetHashCode());
                continue;
            }

            if (isStart) {
                input = RemoveRichTextTag(input, tag, false);
            }

            return input;
        }
    }

}