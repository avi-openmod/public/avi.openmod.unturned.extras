using OpenMod.Unturned.Users;

using Steamworks;

// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedMember.Global
// ReSharper disable UnusedType.Global

namespace Avi.OpenMod.Unturned.Extras.Extensions; 

#pragma warning disable 1591
public static class UnturnedUserExtensions {

#pragma warning restore 1591

    #region Quests

    /// <returns>Player group</returns>
    public static CSteamID GetGroup(this UnturnedUser player)
        => player.Player.GetGroup();

    #endregion

}