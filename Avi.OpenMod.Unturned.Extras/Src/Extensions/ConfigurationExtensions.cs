using System;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;

// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedMember.Global
// ReSharper disable UnusedType.Global

namespace Avi.OpenMod.Unturned.Extras.Extensions; 

/// 
public static class ConfigurationExtensions {

    /// <summary>
    /// Subscribe for config reloads
    /// </summary>
    /// <param name="configuration">the config</param>
    /// <param name="onChange">delegate to run on change</param>
    public static void SubscribeForChanges(this IConfiguration configuration, Action onChange) {
        IChangeToken reloadToken = configuration.GetReloadToken();
        ChangeToken.OnChange(() => reloadToken, onChange);
    }

}