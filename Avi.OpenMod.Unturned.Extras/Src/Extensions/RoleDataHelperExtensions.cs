using System.Threading.Tasks;

using Avi.OpenMod.Unturned.Extras.Services.RoleDataHelper;

using OpenMod.API.Permissions;

using UnityEngine;

// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedMember.Global
// ReSharper disable UnusedType.Global

namespace Avi.OpenMod.Unturned.Extras.Extensions; 

/// 
public static class RoleDataHelperExtensions {

    /// <returns>Role color for given user as Unity color, or null if player doesn't have a role color</returns>
    public static async Task<Color?> GetColorFromRoleData(this IRoleDataHelper roleDataHelper, IPermissionActor actor) {
        string? killerColor = await roleDataHelper.GetRoleDataAsync<string?>(actor, "color");
        return killerColor?.GetUnityColor();
    }

    /// <returns>User role prefix, or null if user has none</returns>
    public static Task<string?> GetPrefixFromRoleData(this IRoleDataHelper roleDataHelper, IPermissionActor actor)
        => roleDataHelper.GetRoleDataAsync<string?>(actor, "prefix");

    /// <returns>User role prefix, or null if user has none</returns>
    public static Task<string?> GetSuffixFromRoleData(this IRoleDataHelper roleDataHelper, IPermissionActor actor)
        => roleDataHelper.GetRoleDataAsync<string?>(actor, "suffix");

}