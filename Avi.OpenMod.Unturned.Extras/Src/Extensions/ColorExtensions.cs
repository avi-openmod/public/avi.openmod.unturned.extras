using System;
using System.Drawing;

using OpenMod.UnityEngine.Extensions;

using SDG.Unturned;

using UnityEngine;

using UColor = UnityEngine.Color;
using SysColor = System.Drawing.Color;

// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedMember.Global
// ReSharper disable UnusedType.Global

namespace Avi.OpenMod.Unturned.Extras.Extensions; 

/// 
public static class ColorExtensions {

    /// <returns>Item asset rarity color</returns>
    public static string GetRarityColor(this ItemAsset asset)
        => asset.rarity.GetRarityColor();

    /// <returns>Vehicle asset rarity color</returns>
    public static string GetRarityColor(this VehicleAsset asset)
        => asset.rarity.GetRarityColor();

    /// <returns>Rarity color for <see cref="EItemRarity"/></returns>
    public static string GetRarityColor(this EItemRarity rarity)
        => ColorUtility.ToHtmlStringRGB(ItemTool.getRarityColorUI(rarity));

    /// <returns>Parsed Unity color, or null if parse failed</returns>
    public static UColor? GetUnityColor(this string colorString) {
        switch (colorString) {

            case "red": return UColor.red;
            case "green": return UColor.green;
            case "blue": return UColor.blue;
            case "white": return UColor.white;
            case "black": return UColor.black;
            case "yellow": return UColor.yellow;
            case "cyan": return UColor.cyan;
            case "magenta": return UColor.magenta;
            case "gray": return UColor.gray;
            case "grey": return UColor.grey;

            default: {
                try {
                    SysColor color = ColorTranslator.FromHtml(colorString);
                    return color.ToUnityColor();
                } catch (Exception) {
                    return null;
                }
            }

        }
    }

    /// <returns>HTML color string for given Unity color</returns>
    public static string ToHtmlString(this UColor color)
        => ColorUtility.ToHtmlStringRGB(color);

}