using OpenMod.Unturned.Users;

// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedMember.Global
// ReSharper disable UnusedType.Global

namespace Avi.OpenMod.Unturned.Extras.Extensions; 

/// 
public static class UnturnedUserDirectoryExtensions {

    /// <returns>Online players count</returns>
    public static int GetOnlineCount(this IUnturnedUserDirectory userDirectory)
        => userDirectory.GetOnlineUsers().Count;

}