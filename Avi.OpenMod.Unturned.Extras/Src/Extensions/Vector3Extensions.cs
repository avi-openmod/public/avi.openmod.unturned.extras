using UnityEngine;

// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedMember.Global
// ReSharper disable UnusedType.Global

namespace Avi.OpenMod.Unturned.Extras.Extensions; 

/// 
public static class Vector3Extensions {

    /// <returns>if two points in space are within specified distance from each other</returns>
    public static bool IsCloseEnough(this Vector3 origin, Vector3 check, float distance)
        => (origin - check).sqrMagnitude < distance * distance;

}