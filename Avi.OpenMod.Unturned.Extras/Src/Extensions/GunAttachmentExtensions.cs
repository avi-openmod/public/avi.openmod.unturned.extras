﻿using System;

using Avi.OpenMod.Unturned.Extras.Models;

using SDG.Unturned;

// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedMember.Global
// ReSharper disable UnusedType.Global
// ReSharper disable UnusedMethodReturnValue.Global

namespace Avi.OpenMod.Unturned.Extras.Extensions; 

/// 
public static class GunAttachmentExtensions {

    /// <summary>
    /// Tries to apply attachment to an item, performing all validity checks
    /// </summary>
    /// <param name="gunItem">the item</param>
    /// <param name="attachmentId">attachment item ID</param>
    /// <param name="attachmentDurability">attachment durability, default full</param>
    /// <returns>true if attachment has been applied, false otherwise</returns>
    public static bool ApplyAttachment(this Item gunItem, ushort attachmentId, byte attachmentDurability = 100) {

        var itemGunAsset = (ItemGunAsset?) Assets.find(EAssetType.ITEM, gunItem.id);
        if (itemGunAsset == null) {
            return false;
        }

        Asset? attachmentAsset = Assets.find(EAssetType.ITEM, attachmentId);

        int[]? indices = attachmentAsset switch {
            ItemBarrelAsset => BarrelIndices,
            ItemGripAsset => GripIndices,
            ItemSightAsset => SightIndices,
            ItemTacticalAsset => TacticalIndices,
            ItemMagazineAsset => MagazineIndices,
            _ => null
        };

        if (indices == null) {
            return false;
        }

        gunItem.ApplyAttachmentByIndices(indices, attachmentId, attachmentDurability);

        return true;
    }

    /// <summary>
    /// Applies attachment to an item with no validity checking
    /// </summary>
    /// <param name="gunItem">the item</param>
    /// <param name="attachmentType">slot for attachment</param>
    /// <param name="attachmentId">attachment item ID</param>
    /// <param name="attachmentDurability">attachment durability, default full</param>
    public static void ForceAttachment(
        this Item gunItem, EAttachmentType attachmentType, ushort attachmentId,
        byte attachmentDurability = 100
    ) {
        int[] indices = GetAttachmentIndices(attachmentType);
        gunItem.ApplyAttachmentByIndices(indices, attachmentId, attachmentDurability);
    }

    // ReSharper disable once SuggestBaseTypeForParameter
    private static void ApplyAttachmentByIndices(this Item gunItem, int[] indices, ushort attachmentId, byte attachmentDurability) {

        byte[] attachIdBytes = BitConverter.GetBytes(attachmentId);

        gunItem.metadata[indices[0]] = attachIdBytes[0];
        gunItem.metadata[indices[1]] = attachIdBytes[1];
        gunItem.metadata[indices[2]] = attachmentDurability;

    }

    /// <summary>
    /// Extracts attachment item from specified slot of a gun item
    /// </summary>
    /// <param name="gunItem">Item to extract attachment from</param>
    /// <param name="attachmentType">attachment type to extract</param>
    /// <returns>Attachment serializable item or null if item is not a gun or the gun has no attachment in specified slot</returns>
    public static SerializableItem? GetAttachmentItem(this Item gunItem, EAttachmentType attachmentType) {

        var itemGunAsset = (ItemGunAsset?) Assets.find(EAssetType.ITEM, gunItem.id);
        if (itemGunAsset == null) {
            return null; // not a gun => no attachment
        }

        int[] indices = GetAttachmentIndices(attachmentType);

        byte[] idContainer = { gunItem.metadata[indices[0]], gunItem.metadata[indices[1]] };

        ushort id = BitConverter.ToUInt16(idContainer, 0);
        if (id == 0) { // 0 means no attachment applied
            return null;
        }

        byte durability = gunItem.metadata[indices[2]];

        return new SerializableItem(id, durability);
    }

    #region Indices

    /// <summary>
    /// Indices of data for sight attachment
    /// </summary>
    public static readonly int[] SightIndices = { 0x00, 0x01, 0x0D };

    /// <summary>
    /// Indices of data for tactical attachment
    /// </summary>
    public static readonly int[] TacticalIndices = { 0x02, 0x03, 0x0E };

    /// <summary>
    /// Indices of data for grip attachment
    /// </summary>
    public static readonly int[] GripIndices = { 0x04, 0x05, 0x0F };

    /// <summary>
    /// Indices of data for barrel attachment
    /// </summary>
    public static readonly int[] BarrelIndices = { 0x06, 0x07, 0x10 };

    /// <summary>
    /// Indices of data for magazine attachment
    /// </summary>
    public static readonly int[] MagazineIndices = { 0x08, 0x09, 0x11 };

    /// <returns>Indices of data for specified attachment type</returns>
    public static int[] GetAttachmentIndices(this EAttachmentType attachmentType) => attachmentType switch {
        EAttachmentType.SIGHT => SightIndices,
        EAttachmentType.TACTICAL => TacticalIndices,
        EAttachmentType.GRIP => GripIndices,
        EAttachmentType.BARREL => BarrelIndices,
        EAttachmentType.MAGAZINE => MagazineIndices,
        _ => throw new ArgumentOutOfRangeException(nameof(attachmentType), attachmentType, null)
    };

    #endregion

}