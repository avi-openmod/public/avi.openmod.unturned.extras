using OpenMod.Unturned.Players;

using SDG.NetTransport;
using SDG.Unturned;

using Steamworks;

using UnityEngine;

// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedMember.Global
// ReSharper disable UnusedType.Global

namespace Avi.OpenMod.Unturned.Extras.Extensions; 

/// 
public static class UnturnedPlayerExtensions {

    #region Equipment

    /// <returns>Player's equipped asset or null if nothing is equipped</returns>
    public static ItemAsset? GetEquippedAsset(this UnturnedPlayer player)
        => player.Player.GetEquippedAsset();

    /// <returns>Player's equipped useable or null if nothing is equipped</returns>
    public static Useable? GetEquippedUseable(this UnturnedPlayer player)
        => player.Player.GetEquippedUseable();

    #endregion

    #region Look

    /// <returns>Player's aim transform</returns>
    public static Transform GetAim(this UnturnedPlayer player)
        => player.Player.GetAim();

    /// <returns>Vehicle in player's aim, or null if there is none within range or blocked</returns>
    public static InteractableVehicle? GetVehicleInAim(this UnturnedPlayer player, float maxRange)
        => player.Player.GetVehicleInAim(maxRange);

    /// <returns>Barricade in player's aim, or null if there is none within range or blocked</returns>
    public static Transform? GetBarricadeInAim(this UnturnedPlayer player, float maxRange)
        => player.Player.GetBarricadeInAim(maxRange);

    /// <returns>Structure in player's aim, or null if there is none within range or blocked</returns>
    public static Transform? GetStructureInAim(this UnturnedPlayer player, float maxRange)
        => player.Player.GetStructureInAim(maxRange);

    /// <returns>Resource in player's aim, or null if there is none within range or blocked</returns>
    public static Transform? GetResourceInAim(this UnturnedPlayer player, float maxRange)
        => player.Player.GetResourceInAim(maxRange);

    #endregion

    #region Position rotation

    /// <returns>Player object position</returns>
    public static Vector3 GetPosition(this UnturnedPlayer player)
        => player.Player.GetPosition();

    /// <returns>Player object rotation</returns>
    public static float GetRotationAngle(this UnturnedPlayer player)
        => player.Player.GetRotationAngle();

    #endregion

    #region Quests

    /// <returns>Player group</returns>
    public static CSteamID GetGroup(this UnturnedPlayer player)
        => player.Player.GetGroup();

    #endregion

    #region Transport connection

    /// <returns>Player's transport connection for RPC usage</returns>
    public static ITransportConnection GetTransportConnection(this UnturnedPlayer player)
        => player.Player.GetTransportConnection();

    #endregion

}