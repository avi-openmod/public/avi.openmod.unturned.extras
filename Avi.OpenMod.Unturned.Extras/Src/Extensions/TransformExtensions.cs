using UnityEngine;

// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedMember.Global
// ReSharper disable UnusedType.Global

namespace Avi.OpenMod.Unturned.Extras.Extensions; 

/// 
public static class TransformExtensions {

    /// <param name="tf">transform</param>
    /// <returns>true if transform is a barricade</returns>
    public static bool IsBarricade(this Transform tf)
        => tf.CompareTag("Barricade");

    /// <param name="tf">transform</param>
    /// <returns>true if transform is a structure</returns>
    public static bool IsStructure(this Transform tf)
        => tf.CompareTag("Structure");

    /// <param name="tf">transform</param>
    /// <returns>true if transform is a resource</returns>
    public static bool IsResource(this Transform tf)
        => tf.CompareTag("Resource");

}