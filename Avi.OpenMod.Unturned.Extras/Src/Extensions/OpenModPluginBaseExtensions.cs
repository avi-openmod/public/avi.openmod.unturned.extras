using System;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

using OpenMod.Core.Plugins;
using OpenMod.Core.Plugins.Events;

// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedMember.Global
// ReSharper disable UnusedType.Global

namespace Avi.OpenMod.Unturned.Extras.Extensions; 

/// 
public static class OpenModPluginBaseExtensions {

    private static Func<OpenModPluginBase, ILogger> GenerateLoggerGetterLambda() {
        PropertyInfo property = typeof(OpenModPluginBase).GetProperty("Logger", BindingFlags.Instance | BindingFlags.NonPublic)!;
        ParameterExpression parameterExpr = Expression.Parameter(typeof(OpenModPluginBase), "instance");
        UnaryExpression instanceExpr = Expression.TypeAs(parameterExpr, typeof(OpenModPluginBase));
        MemberExpression body = Expression.Property(instanceExpr, property);
        return Expression.Lambda<Func<OpenModPluginBase, ILogger>>(body, parameterExpr).Compile();
    }

    private static readonly Lazy<Func<OpenModPluginBase, ILogger>> LazyPluginLogger = new(GenerateLoggerGetterLambda);

    /// <returns>plugin logger, accessible from anywhere</returns>
    public static ILogger GetLogger(this OpenModPluginBase plugin)
        => LazyPluginLogger.Value(plugin);

    /// <summary>
    /// Gets value from config and ensures it's not less than min
    /// </summary>
    public static T GetConfigValueWithMin<T>(this OpenModPluginBase plugin, string key, T min, T defaultValue, string warnText)
        where T : IComparable<T>
        => plugin.GetConfigValueWithMin(plugin.Configuration, key, min, defaultValue, warnText);

    /// <summary>
    /// Gets value from config and ensures it's not less than min
    /// </summary>
    public static T GetConfigValueWithMin<T>(
        this OpenModPluginBase plugin, IConfiguration configuration, string key, T min, T defaultValue, string warnText
    )
        where T : IComparable<T> {

        var value = configuration.GetValue<T>(key);

        if (value.CompareTo(min) >= 0) {
            return value;
        }

        plugin.GetLogger()
            .LogWarning("{WarnText} in config < {Min}, defaulting to {Default}", warnText, min, defaultValue);

        return min;

    }

    /// <summary>
    /// Gets value from config and ensures it's not more than max
    /// </summary>
    public static T GetConfigValueWithMax<T>(this OpenModPluginBase plugin, string key, T max, T defaultValue, string warnText)
        where T : IComparable<T>
        => plugin.GetConfigValueWithMin(plugin.Configuration, key, max, defaultValue, warnText);

    /// <summary>
    /// Gets value from config and ensures it's not more than max
    /// </summary>
    public static T GetConfigValueWithMax<T>(
        this OpenModPluginBase plugin, IConfiguration configuration, string key, T max, T defaultValue, string warnText
    )
        where T : IComparable<T> {

        var value = configuration.GetValue<T>(key);

        if (value.CompareTo(max) <= 0) {
            return value;
        }

        plugin.GetLogger()
            .LogWarning("{WarnText} in config > {Max}, defaulting to {Default}", warnText, max, defaultValue);

        return max;

    }

    /// <summary>
    /// Gets value from config clamped within a specified range
    /// </summary>
    public static T GetConfigValueWithMinMax<T>(this OpenModPluginBase plugin, string key, T min, T max, T defaultValue, string warnText)
        where T : IComparable<T>
        => plugin.GetConfigValueWithMinMax(plugin.Configuration, key, min, max, defaultValue, warnText);

    /// <summary>
    /// Gets value from config clamped within a specified range
    /// </summary>
    public static T GetConfigValueWithMinMax<T>(
        this OpenModPluginBase plugin, IConfiguration configuration, string key, T min, T max, T defaultValue, string warnText
    )
        where T : IComparable<T> {

        var value = configuration.GetValue<T>(key);

        ILogger logger = plugin.GetLogger();
        if (value.CompareTo(min) < 0) {
            logger.LogWarning("{WarnText} in config < {Min}, defaulting to {Default}", warnText, min, defaultValue);
            return min;
        }

        if (value.CompareTo(max) > 0) {
            logger.LogWarning("{WarnText} in config > {Max}, defaulting to {Default}", warnText, max, defaultValue);
            return min;
        }

        return value;

    }

    /// <summary>
    /// Gets a float value from config and ensures it's not less than min or is negative
    /// </summary>
    public static float GetConfigValueWithMinOrNegative(
        this OpenModPluginBase plugin, string key, float min, float defaultValue, string warnText
    )
        => plugin.GetConfigValueWithMinOrNegative(plugin.Configuration, key, min, defaultValue, warnText);

    /// <summary>
    /// Gets a float value from config and ensures it's not less than min or is negative
    /// </summary>
    public static float GetConfigValueWithMinOrNegative(
        this OpenModPluginBase plugin, IConfiguration configuration, string key, float min, float defaultValue, string warnText
    ) {

        if (min <= 0) {
            throw new ArgumentException("Min must be > 0", nameof(min));
        }

        float value = configuration.GetValue<float>(key);

        if (value < 0) {
            return value;
        }

        ILogger logger = plugin.GetLogger();
        if (value < min) {
            logger.LogWarning("{WarnText} in config < {Min} and not negative, defaulting to {Default}", warnText, min, defaultValue);
            return min;
        }

        return value;

    }

    /// <summary>
    /// Runs a task when config changes
    /// </summary>
    public static void SubscribeToConfigChanges(this OpenModPluginBase plugin, Func<Task> callback)
        => plugin.EventBus.Subscribe<PluginConfigurationChangedEvent>(plugin, async (_, _, ev) => {
            if (plugin.OpenModComponentId == ev.Plugin.OpenModComponentId) {
                await callback();
            }
        });

}