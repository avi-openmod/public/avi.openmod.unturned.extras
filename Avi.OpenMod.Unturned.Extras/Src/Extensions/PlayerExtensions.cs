using System;

using SDG.NetTransport;
using SDG.Unturned;

using Steamworks;

using UnityEngine;

// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedType.Global
// ReSharper disable UnusedMember.Global

namespace Avi.OpenMod.Unturned.Extras.Extensions; 

#pragma warning disable 1591
public static class PlayerExtensions {

#pragma warning restore 1591

    #region Equipment

    /// <returns>Player's equipped asset or null if nothing is equipped</returns>
    public static ItemAsset? GetEquippedAsset(this Player player)
        => player.equipment.asset;

    /// <returns>Player's equipped useable or null if nothing is equipped</returns>
    public static Useable? GetEquippedUseable(this Player player)
        => player.equipment.useable;

    #endregion

    #region Look

    /// <returns>Player's aim transform</returns>
    public static Transform GetAim(this Player player)
        => player.look.aim;

    /// <summary>
    /// Default interaction ray masks:
    /// enemy, item, small, medium, large, resource, vehicle, barricade, structure, entity, ground
    /// </summary>
    public const int DEFAULT_INTERACTION_RAY_MASKS = RayMasks.ENEMY |
                                                     RayMasks.ITEM |
                                                     RayMasks.SMALL |
                                                     RayMasks.MEDIUM |
                                                     RayMasks.LARGE |
                                                     RayMasks.RESOURCE |
                                                     RayMasks.VEHICLE |
                                                     RayMasks.BARRICADE |
                                                     RayMasks.STRUCTURE |
                                                     RayMasks.ENTITY |
                                                     RayMasks.GROUND;

    /// <param name="player">player to check aim for</param>
    /// <param name="maxRange">max range of check</param>
    /// <param name="rayMasks">optional ray masks, or default interaction masks if not specified</param>
    /// <returns>Raycast info for player's aim</returns>
    /// <exception cref="ArgumentOutOfRangeException">when range is not positive</exception>
    public static RaycastInfo RaycastInAim(this Player player, float maxRange, int rayMasks = DEFAULT_INTERACTION_RAY_MASKS) {

        if (maxRange <= 0f) {
            throw new ArgumentOutOfRangeException(nameof(maxRange), "Range must be positive");
        }

        ThreadUtil.assertIsGameThread();

        Transform aim = player.look.aim;
        var aimRay = new Ray(aim.position, aim.forward);
        return DamageTool.raycast(aimRay, maxRange, rayMasks, player);
    }

    /// <param name="player">player to check aim for</param>
    /// <param name="maxRange">max range of check</param>
    /// <param name="rayMasks">optional ray masks, or default interaction masks if not specified</param>
    /// <returns>Vehicle in player's aim, or null if there is none within range or blocked</returns>
    public static InteractableVehicle? GetVehicleInAim(this Player player, float maxRange, int rayMasks = DEFAULT_INTERACTION_RAY_MASKS) {
        RaycastInfo raycastInfo = player.RaycastInAim(maxRange, rayMasks);
        return raycastInfo.vehicle;
    }

    /// <param name="player">player to check aim for</param>
    /// <param name="maxRange">max range of check</param>
    /// <param name="rayMasks">optional ray masks, or default interaction masks if not specified</param>
    /// <returns>Barricade in player's aim, or null if there is none within range or blocked</returns>
    public static Transform? GetBarricadeInAim(this Player player, float maxRange, int rayMasks = DEFAULT_INTERACTION_RAY_MASKS) {

        RaycastInfo raycastInfo = player.RaycastInAim(maxRange, rayMasks);
        if (raycastInfo.transform == null || !raycastInfo.transform.IsBarricade()) {
            return null;
        }

        return raycastInfo.transform;
    }

    /// <param name="player">player to check aim for</param>
    /// <param name="maxRange">max range of check</param>
    /// <param name="rayMasks">optional ray masks, or default interaction masks if not specified</param>
    /// <returns>Structure in player's aim, or null if there is none within range or blocked</returns>
    public static Transform? GetStructureInAim(this Player player, float maxRange, int rayMasks = DEFAULT_INTERACTION_RAY_MASKS) {

        RaycastInfo raycastInfo = player.RaycastInAim(maxRange, rayMasks);
        if (raycastInfo.transform == null || !raycastInfo.transform.IsStructure()) {
            return null;
        }

        return raycastInfo.transform;
    }

    /// <param name="player">player to check aim for</param>
    /// <param name="maxRange">max range of check</param>
    /// <param name="rayMasks">optional ray masks, or default interaction masks if not specified</param>
    /// <returns>Resource in player's aim, or null if there is none within range or blocked</returns>
    public static Transform? GetResourceInAim(this Player player, float maxRange, int rayMasks = DEFAULT_INTERACTION_RAY_MASKS) {

        RaycastInfo raycastInfo = player.RaycastInAim(maxRange, rayMasks);
        if (raycastInfo.transform == null || !raycastInfo.transform.IsResource()) {
            return null;
        }

        return raycastInfo.transform;
    }

    #endregion

    #region Position rotation

    /// <returns>Player object position</returns>
    public static Vector3 GetPosition(this Player player)
        => player.transform.position;

    /// <returns>Player object rotation</returns>
    public static float GetRotationAngle(this Player player)
        => player.transform.eulerAngles.y;

    #endregion

    #region Quests

    /// <returns>Player group</returns>
    public static CSteamID GetGroup(this Player player)
        => player.quests.groupID;

    #endregion

    #region Transport connection

    /// <returns>Player's transport connection for RPC usage</returns>
    public static ITransportConnection GetTransportConnection(this Player player)
        => player.channel.owner.transportConnection;

    #endregion

}