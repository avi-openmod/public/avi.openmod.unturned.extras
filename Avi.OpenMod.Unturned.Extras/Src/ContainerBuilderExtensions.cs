using Autofac;

using Avi.OpenMod.Unturned.Extras.Services.RoleDataHelper;
using Avi.OpenMod.Unturned.Extras.Services.WorkshopChecker;

// ReSharper disable UnusedMember.Global
// ReSharper disable UnusedType.Global

namespace Avi.OpenMod.Unturned.Extras;

#pragma warning disable CS1591
public static class ContainerBuilderExtensions {

#pragma warning restore CS1591

    /// <summary>
    /// Sets up container for <see cref="IRoleDataHelper"/> usage
    /// </summary>
    public static void AddRoleDataHelperSupport(this ContainerBuilder builder)
        => builder.RegisterType<RoleDataHelper>()
            .As<IRoleDataHelper>()
            .InstancePerLifetimeScope()
            .OwnedByLifetimeScope();

    /// <summary>
    /// Sets up container for <see cref="IWorkshopEffectChecker"/> usage
    /// </summary>
    public static void AddWorkshopEffectCheckerSupport(this ContainerBuilder builder)
        => builder.RegisterType<WorkshopEffectChecker>()
            .As<IWorkshopEffectChecker>()
            .InstancePerDependency()
            .OwnedByLifetimeScope();

}