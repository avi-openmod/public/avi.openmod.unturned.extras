﻿using System;
using System.ComponentModel;

using Avi.OpenMod.Unturned.Extras.Extensions;

using SDG.Unturned;

// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global
// ReSharper disable UnusedType.Global
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedMember.Global

namespace Avi.OpenMod.Unturned.Extras.Models; 

/// <summary>
/// Serializable class that holds necessary data to spawn Unturned item
/// </summary>
public class SerializableItem {

    #region All items

    /// <summary>
    /// Item ID
    /// </summary>
    public ushort Id { get; set; }

    /// <summary>
    /// Item durability
    /// </summary>
    [DefaultValue(100)]
    public byte Durability { get; set; } = 100;

    #endregion

    #region Fuel

    /// <summary>
    /// Fuel item fill percentage
    /// </summary>
    public float? FuelPercentage { get; set; }

    #endregion

    #region Guns

    /// <summary>
    /// Barrel weapon attachment
    /// </summary>
    public SerializableItem? Barrel { get; set; }

    /// <summary>
    /// Sight weapon attachment
    /// </summary>
    public SerializableItem? Sight { get; set; }

    /// <summary>
    /// Grip weapon attachment
    /// </summary>
    public SerializableItem? Grip { get; set; }

    /// <summary>
    /// Magazine weapon attachment
    /// </summary>
    public SerializableItem? Magazine { get; set; }

    /// <summary>
    /// Tactical weapon attachment
    /// </summary>
    public SerializableItem? Tactical { get; set; }

    /// <summary>
    /// Current weapon fire mode
    /// </summary>
    public EFiremode? Firemode { get; set; }

    /// <summary>
    /// Sight weapon attachment
    /// </summary>
    public byte? Ammo { get; set; }

    #endregion

    /// 
    public SerializableItem() { }

    /// 
    public SerializableItem(ushort id, byte durability = 100) {
        Id = id;
        Durability = durability;
    }

    /// 
    public SerializableItem(Item item) {

        if (item == null) {
            throw new ArgumentNullException(nameof(item));
        }

        Id = item.id;
        Durability = item.durability;

        var asset = (ItemAsset?) Assets.find(EAssetType.ITEM, Id);

        switch (asset) {

            case ItemFuelAsset fuelAsset: {
                FuelPercentage = BitConverter.ToInt16(item.metadata, 0) / (float) fuelAsset.fuel;
                break;
            }

            case ItemGunAsset: {

                Ammo = item.metadata[10];

                Firemode = (EFiremode) item.metadata[11];

                Sight = item.GetAttachmentItem(EAttachmentType.SIGHT);
                Tactical = item.GetAttachmentItem(EAttachmentType.TACTICAL);
                Grip = item.GetAttachmentItem(EAttachmentType.GRIP);
                Barrel = item.GetAttachmentItem(EAttachmentType.BARREL);
                Magazine = item.GetAttachmentItem(EAttachmentType.MAGAZINE);

                break;
            }

        }

    }

    private Item CreateItem() {

        var item = new Item(Id, EItemOrigin.ADMIN, Durability);

        var asset = (ItemAsset?) Assets.find(EAssetType.ITEM, Id);

        switch (asset) {

            case null: {
                break;
            }

            case ItemFuelAsset fuelAsset: {

                uint fuel;
                switch (FuelPercentage) {
                    case null:
                    case >= 100: {
                        fuel = fuelAsset.fuel;
                        break;
                    }

                    case <= 0: {
                        fuel = 0;
                        break;
                    }

                    default: {
                        fuel = (uint) Math.Floor(fuelAsset.fuel * FuelPercentage.Value / 100.0f);
                        break;
                    }
                }

                item.metadata[0] = (byte) (fuel % 256);
                item.metadata[1] = (byte) (fuel / 256);

                break;
            }

            case ItemGunAsset: {

                if (Ammo != null) {
                    item.metadata[10] = Ammo.Value;
                }

                if (Firemode != null) {
                    item.metadata[11] = (byte) Firemode;
                }

                if (Sight != null) {
                    item.ApplyAttachment(Sight.Id, Sight.Durability);
                }

                if (Tactical != null) {
                    item.ApplyAttachment(Tactical.Id, Tactical.Durability);
                }

                if (Grip != null) {
                    item.ApplyAttachment(Grip.Id, Grip.Durability);
                }

                if (Barrel != null) {
                    item.ApplyAttachment(Barrel.Id, Barrel.Durability);
                }

                if (Magazine != null) {
                    item.ApplyAttachment(Magazine.Id, Magazine.Durability);
                }

                break;
            }

        }

        return item;
    }

    /// <returns>true if serializable item represents the Unturned Item exactly</returns>
    public static explicit operator Item?(SerializableItem? item)
        => item?.CreateItem();

    /// <returns>true if serializable item represents the Unturned Item exactly</returns>
    public static explicit operator SerializableItem?(Item? item)
        => item == null ? null : new SerializableItem(item);

}