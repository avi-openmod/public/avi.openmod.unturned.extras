﻿namespace Avi.OpenMod.Unturned.Extras.Models; 

/// <summary>
/// Unturned weapon attachment types
/// </summary>
public enum EAttachmentType {

#pragma warning disable 1591
    SIGHT,
    TACTICAL,
    GRIP,
    BARREL,
    MAGAZINE
#pragma warning restore 1591

}